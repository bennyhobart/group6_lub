package com.lub;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ToggleButton;

import com.lub.friend.activities.FriendsListActivity;
import com.lub.friend.database.UpdateUser;
import com.lub.message.MessageHandler;
import com.lub.messenger.AlertActivity;
import com.lub.messenger.AlertCancelledActivity;
import com.lub.messenger.MessengerActivity;
import com.lub.user.ProfileActivity;

/**
 * User: BenedictHobart
 * Date: 26/08/13
 * Time: 11:49 AM
 * 
 * Handles the menu screen on the application
 * 
 * MenuActivity handles events for the various buttons on the menu screen,
 * such as friends, messages, options, logout and alert.
 */
public class MenuActivity extends Activity {
    private Button friends;
    private Button messages;
    private Button profile;
    private Button logout;
    private ToggleButton alert;
    private int status;
    private UpdateUser updateUser = new UpdateUser();
    private MessageHandler messageHandler = new MessageHandler();
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        
        // Link to FriendsList when friend button is tapped
        friends = (Button)findViewById(R.id.friends_menu);
        friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i;
                i = new Intent(MenuActivity.this, FriendsListActivity.class);
                startActivity(i);
            }
        });

        //Sends ok message if status is not already ok
        messages = (Button)findViewById(R.id.messages_menu);
        messages.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            	if(messageHandler.sendOkMessage()){
            		Intent i = new Intent(MenuActivity.this, MessengerActivity.class);
            		startActivity(i);
            	}
            }
        });

        // Link to profile screen when profile button is tapped.
        profile = (Button)findViewById(R.id.profile_menu);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i;
                i = new Intent(MenuActivity.this, ProfileActivity.class);
                startActivity(i);
            }
        });

        // Send an alert when alert button is pressed
        status =  updateUser.getCurrentStatus();
        alert = (ToggleButton)findViewById(R.id.alert_menu);
        if (status == R.integer.ok){
        	alert.setChecked(false);
        }
        else{
        	alert.setChecked(true);
        }
        alert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
				if (alert.isChecked()) {
					messageHandler.sendAlert();
            		startActivity(new Intent(MenuActivity.this, AlertActivity.class));
				} else {
					messageHandler.cancelAlert();
            		startActivity(new Intent(MenuActivity.this, AlertCancelledActivity.class));
				}
            }
        });

        // Logout the current user when logout button tapped
        logout = (Button)findViewById(R.id.logout_menu);
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateUser.logOut();
                finish();
            }
        });
    }
        
	        @Override
	        public void onResume() {
	        	super.onResume();
	        	status =  updateUser.getCurrentStatus();
	            if (status == R.integer.ok){
	            	alert.setChecked(false);
	            }
	            else{
	            	alert.setChecked(true);
	            }
	        }
    }

