/**
Created by Leanne Capewell
Email: capewell.leanne@gmail.com
Created on 08/09/2013
*/


package com.lub;

import android.app.Application;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.testflightapp.lib.TestFlight;

public class LubApplication extends Application {

	public void onCreate(){
		 Parse.initialize(this, "qdqTq8zSoEPASkfogqkDIfodCpHPznGfZjhGj1QF", "9AOG9laTXfF7JK8YewOhpVEXwPn9ny69lKsRb9D6");
		 TestFlight.takeOff(this, "3e70c3af-64ad-4e89-95c1-c6802ab305d5");
		 PushService.setDefaultPushCallback(this, MainActivity.class);
		 ParseInstallation.getCurrentInstallation().saveInBackground();

	}
	
}
