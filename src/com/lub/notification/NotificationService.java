package com.lub.notification;

import java.util.ArrayList;

import com.lub.friend.database.FriendGroup;
import com.lub.friend.database.UpdateUser;
import com.lub.friend.model.Friend;
import com.lub.user.Person;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseQuery;

/**
 * Created by Leanne Capewell Email: capewell.leanne@gmail.com Created on
 * 03/10/2013
 * 
 * Service for sending push notifications to users
 */
public class NotificationService {

	private UpdateUser updateUser;

	/**
	 * Constructor
	 * 
	 */
	public NotificationService() {
		this.updateUser = new UpdateUser();
	}

	/**
	 * Constructor for tests
	 * 
	 * @param updateUser
	 */
	public NotificationService(UpdateUser updateUser) {
		this.updateUser = updateUser;
	}

	/**
	 * Sends the given message as a push-notification to each of the current
	 * user's friends
	 * 
	 * @param message
	 *            The message to be sent in the notification
	 */
	public void pushNotificationToAllFriends(String message) {
		ArrayList<Friend> friends = FriendGroup.get().getFriends();
		ParseQuery<ParseInstallation> pushQuery = ParseInstallation.getQuery();
		String fullMessage = addNameToMessage(message);
		for (Friend friend : friends) {
			pushQuery.whereEqualTo("username", friend.getUsername());
			// Send push notification if friend is set as a carer
			if (friend.isCarer()){
				ParsePush push = new ParsePush();
				push.setQuery(pushQuery);
				push.setMessage(fullMessage);
				push.sendInBackground();
			}
		}
	}

	/**
	 * Sends the given message as a push-notification to the given user
	 * 
	 * @param message
	 *            The message to be sent in the notification
	 */
	public void pushNotificationToUser(String username, String message) {
		ParseQuery<ParseInstallation> pushQuery = ParseInstallation.getQuery();
		String fullMessage = addNameToMessage(message);
		pushQuery.whereEqualTo("username", username);
		// Send push notification
		ParsePush push = new ParsePush();
		push.setQuery(pushQuery);
		push.setMessage(fullMessage);
		push.sendInBackground();
	}

	// returns the current user's full name appended to the required message
	private String addNameToMessage(String text) {
		Person person = updateUser.getCurrentPerson();
		String message = person.getFirstName() + " " + person.getLastName();
		return message.concat(text);
	}
}
