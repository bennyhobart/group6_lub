package com.lub.messenger;

import com.lub.R;
import com.lub.friend.database.FriendGroup;
import com.lub.friend.model.Friend;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created with IntelliJ IDEA.
 * User: BenedictHobart
 * Date: 28/08/13
 * Time: 10:38 AM
 */
public class MessageDetailActivity extends Activity {
    public static final String EXTRA_FRIEND_ID = "com.lub.friend_id";
    private Friend friend;
    TextView textMessage;
    private Button send;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String friendId = (String)getIntent().getSerializableExtra(EXTRA_FRIEND_ID);
        setContentView(R.layout.activity_message_detail);
        // Get the friend object
        friend = FriendGroup.get().getFriend(friendId);
        textMessage = (TextView)findViewById(R.id.msg_edit);

        send = (Button)findViewById(R.id.send_btn);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                
            }
		});
    }

}
