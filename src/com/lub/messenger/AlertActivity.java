package com.lub.messenger;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.lub.R;

/**
 * Created by Leanne Capewell 
 * Email: capewell.leanne@gmail.com 
 * Created on 03/10/2013
 * 
 * Displays an acknowledgment that an alert has been sent Clicking ok button
 * returns user to menu activity
 */
public class AlertActivity extends Activity {
	private Button confirm;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_alert);

		// Link to FriendsList when friend button is tapped
		confirm = (Button) findViewById(R.id.ok_button);
		confirm.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
}
