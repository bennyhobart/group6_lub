package com.lub.messenger;

import java.util.ArrayList;

import com.lub.R;
import com.lub.friend.activities.FriendDetailActivity;
import com.lub.friend.database.FriendGroup;
import com.lub.friend.model.Friend;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
/**
 * Created with IntelliJ IDEA.
 * User: BenedictHobart
 * Date: 28/08/13
 * Time: 9:33 AM
 * 
 * Activity for messaging screen
 */
public class MessageListFragment extends ListFragment
{
	ArrayList<Friend> friends;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		friends = FriendGroup.get().getFriends();
		FriendAdapter adapter = new FriendAdapter(friends);
		setListAdapter(adapter);
	}
	
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Friend f = ((FriendAdapter)getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(),MessageDetailActivity.class);
        i.putExtra(FriendDetailActivity.EXTRA_FRIEND_ID,f.getId());
        startActivity(i);
    }
	
    private class FriendAdapter extends ArrayAdapter<Friend> {
        public FriendAdapter(ArrayList<Friend> friends) {
            super(getActivity(),0,friends);
        }
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            if(convertView == null) {
                convertView = getActivity().getLayoutInflater().inflate(R.layout.fragment_friend,null);
            }
            Friend f = getItem(position);
            TextView name = (TextView)convertView.findViewById(R.id.name_friend);
            name.setText(f.getFirstName() + " " + f.getLastName());
            TextView address = (TextView)convertView.findViewById(R.id.detail_friend);
            address.setText(f.getAddress());
            return convertView;
        }
    }
}
