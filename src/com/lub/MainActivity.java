package com.lub;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import com.lub.login.LoginActivity;
import com.parse.ParseAnalytics;
import com.parse.ParseUser;

public class MainActivity extends Activity {
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        ParseAnalytics.trackAppOpened(getIntent());
        startActivity(new Intent(MainActivity.this,LoginActivity.class));

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        ParseUser.logOut();

    }
}
