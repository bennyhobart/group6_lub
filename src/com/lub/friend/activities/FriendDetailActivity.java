package com.lub.friend.activities;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

import com.lub.R;
import com.lub.friend.database.FriendGroup;
import com.lub.friend.model.Friend;

/**
 * Created with IntelliJ IDEA.
 * User: BenedictHobart
 * Date: 28/08/13
 * Time: 10:38 AM
 */

/**
 * This activity lists the details of a friend
 * It's layout can be found in res/activity_friend_detail
 */
public class FriendDetailActivity extends Activity {
    public static final String EXTRA_FRIEND_ID = "com.lub.friend_id";
    //Target Friend
    private Friend friend;

    //layout objects
    private TextView name;
    private TextView address;
    private TextView email;
    private TextView phoneNumber;
    private TextView username;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get passed friend ID
        String friendId = (String)getIntent().getSerializableExtra(EXTRA_FRIEND_ID);
        setContentView(R.layout.activity_friend_detail);
        // Get the friend object
        friend = FriendGroup.get().getFriend(friendId);
        // Populate each field based on the friend object

        name = (TextView)findViewById(R.id.name_friend_detail);
        name.setText(friend.getFirstName() + " " + friend.getLastName());

        username = (TextView)findViewById(R.id.username_friend_detail);
        username.setText(friend.getUsername());

        address = (TextView)findViewById(R.id.address_friend_detail);
        address.setText(friend.getAddress());

        email = (TextView)findViewById(R.id.email_friend_detail);
        email.setText(friend.getEmail());

        phoneNumber = (TextView)findViewById(R.id.phone_number_friend_detail);
        phoneNumber.setText(friend.getPhoneNumber());
    }
}
