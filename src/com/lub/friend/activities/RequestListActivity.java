package com.lub.friend.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;

import com.lub.R;

/**
 * Created with IntelliJ IDEA.
 * User: BenedictHobart
 * Date: 26/08/13
 * Time: 12:17 PM
 * To change this template use File | Settings | File Templates.
 */

/**
 * This activity lists the current users friends in a readable and interactive manner
 * it populates the list with a FriendListFragment and a RequestListFragment
 */
public class RequestListActivity extends FragmentActivity
{
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_requests_list);
        FragmentManager fm = getSupportFragmentManager();
        Fragment requestFragment = fm.findFragmentById(R.id.requestContainer);
        //create request fragment
        if(requestFragment == null ) {
            requestFragment = new RequestListFragment();
            fm.beginTransaction()
                    .add(R.id.requestContainer, requestFragment)
                    .commit();
        }
    }

}
