package com.lub.friend.database;

import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created with IntelliJ IDEA.
 * User: BenedictHobart
 * Date: 11/09/13
 * Time: 11:38 AM
 */
/**
 * simple model class representing a request for a friend in the database
 */
public class Request {
    private ParseObject requester;
    private String name;
    
    public Request(ParseObject person) {
        requester = person;
        name = person.getString("first_name") + " " + person.getString("last_name");
    }
    
    public Request() {
        requester = ParseUser.getCurrentUser();
        name = requester.getString("first_name") + " " + requester.getString("last_name");
    }
    
    /**
     * Get the full name of the user in the request
     * @return The user's full name
     */
    public String getName() {
        return name;
    }

	public ParseObject getRequester() {
		return requester;
	}
    
}
