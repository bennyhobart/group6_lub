package com.lub.friend.model;

import com.lub.R;
import com.parse.ParseObject;

/**
 * Created with IntelliJ IDEA. User: BenedictHobart Date: 27/08/13 Time: 11:08
 * AM Model layer class representing a Person's friend, used for interaction
 * between a person and friend
 */

/**
 * simple model class representing a friend user in the database
 */
public class Friend {
	private String username;
	private String email;
	private String firstName;
	private String lastName;
	private String address;
	private String phoneNumber;
	private String id;
	private int status;
	private boolean isCarer;

	public String getId() {
		return id;
	}

	public Friend() {
		username = "BennysGenericUsername";
		email = "BennysGenericEmail";
		firstName = "Joe";
		lastName = "Blow";
		address = "1234 5 6 Essendon Station";
		phoneNumber = "unimplemented";
		id = "";
		status = R.integer.ok;
		isCarer = true;
	}

	public int getColour() {
		if (status == R.integer.ok) {
			return R.color.green;
		}
		if (status == R.integer.notOk) {
			return R.color.orange;
		} else {
			return R.color.red;
		}
	}

	public Friend(ParseObject friend) {
		username = friend.getString("username");
		email = friend.getString("email");
		firstName = friend.getString("first_name");
		lastName = friend.getString("last_name");
		address = friend.getString("address");
		phoneNumber = friend.getString("phoneNumber");
		id = friend.getObjectId();
		status = friend.getInt("status");
		isCarer = friend.getBoolean("carer");
	}

	public boolean isCarer() {
		return isCarer;
	}
	
	public void setCarer(boolean isCarer) {
		this.isCarer = isCarer;
	}
	
	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

}
