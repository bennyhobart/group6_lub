
/**
 * 
 */
package com.lub.friend.database;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.anyString;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;

import android.test.InstrumentationTestCase;

import com.lub.notification.NotificationService;
import com.parse.ParseObject;

/**
 * Created by Leanne Capewell
 * Email: capewell.leanne@gmail.com
 * Created on 21/10/2013
 * 
 * Unit tests the RequestHandler class
 */
public class RequestHandlerTest extends InstrumentationTestCase {
	
	private ParseObject parseObjectMock;
	private Request requestMock;
	private NotificationService notificationMock;
	private Requests requestsMock;
	private String username = "username";
	private RequestHandler requestHandler;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		super.setUp();
		System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
		notificationMock = mock(NotificationService.class);
		parseObjectMock = mock(ParseObject.class);
		requestsMock = mock(Requests.class);
		requestMock = mock(Request.class);
		when(requestMock.getRequester()).thenReturn(parseObjectMock);
		requestHandler = new RequestHandler(notificationMock, requestsMock);
	}

	/**
	 * Test the sendRequest method, and check that it calls the pushNotificationToUser() method
	 * @throws Exception
	 */
	@Test
	public final void testSendRequest() throws Exception {
		when(requestsMock.friendRequest(username)).thenReturn(true);
		requestHandler.sendRequest(username);
        verify(notificationMock).pushNotificationToUser(username, " has sent you a friend request");
	}
	
	/**
	 * Test the sendRequest method, and check that it does not call the pushNotificationToUser() method
	 * when the username being friend requested does not exist
	 * @throws Exception
	 */
	@Test
	public final void testSendRequestWhenUsernameDoesNotExist() throws Exception {
		when(requestsMock.friendRequest(username)).thenReturn(false);
		requestHandler.sendRequest(username);
        verify(notificationMock, never()).pushNotificationToUser(anyString(), anyString());
	}

	/**
	 * Test the acceptRequest method
	 * @throws Exception
	 */
	@Test
	public final void testAcceptRequest() throws Exception {
		requestHandler.acceptRequest(requestMock);
		verify(requestsMock).acknowledgeRequest(parseObjectMock);
	}

	/**
	 * Test the decline request method
	 * @throws Exception
	 */
	@Test
	public final void testDeclineRequest() throws Exception {
		requestHandler.declineRequest(requestMock);
		verify(requestsMock).ignoreRequest(parseObjectMock);
	}

}
