/**
 * 
 */
package com.lub.friend.database;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import android.test.InstrumentationTestCase;

import com.parse.ParseObject;

/**
 * Created by Leanne Capewell
 * Email: capewell.leanne@gmail.com
 * Created on 20/10/2013
 * 
 * Unit tests the Request class
 */
@RunWith(MockitoJUnitRunner.class)
public class RequestTest extends InstrumentationTestCase{
	
	private ParseObject parseObjectMock;
	private Request request;
	private String firstName = "Bob";
	private String lastName = "Smith";
	private String fullName = firstName + " " + lastName;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		super.setUp();
		System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
		parseObjectMock = mock(ParseObject.class);
		when(parseObjectMock.getString("first_name")).thenReturn(firstName);
		when(parseObjectMock.getString("last_name")).thenReturn(lastName);
		request = new Request(parseObjectMock);
	}

	/**
	 * Test the getName method
	 * @throws Exception
	 */
	@Test
	public final void testGetName() throws Exception {
		String name = request.getName();
		assertEquals(name, fullName);
	}
}
