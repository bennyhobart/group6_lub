/**
Created by Leanne Capewell
Email: capewell.leanne@gmail.com
Created on 22/09/2013
*/

/**
 * Tests the UpdateUser class to ensure the correct user details
 * are added to the database.
 */
package com.lub.friend.database;


import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Test;

import android.test.InstrumentationTestCase;

import com.lub.parse.ParseWrapper;
import com.parse.ParseUser;

/**
 * Unit tests for the UpdateUser class
 */
public class UpdateUserTest extends InstrumentationTestCase{

	private ParseUser userMock;
	private ParseWrapper parseWrapperMock;
	private UpdateUser updateUser;
	private int ok;
	
	/**
	 * @throws java.lang.Exception
	 */

	public void setUp() throws Exception {
		super.setUp();
		System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
		userMock = mock(ParseUser.class);
		parseWrapperMock = mock(ParseWrapper.class);
		updateUser = new UpdateUser(parseWrapperMock);
	    doNothing().when(userMock).put(any(String.class), anyInt());
	    doNothing().when(userMock).saveInBackground();
	    ok = 0;
	}
	
	/**
	 * Test the setStatus method
	 */
	@Test
	public void testSetStatus(){
		when(parseWrapperMock.getCurrentUser()).thenReturn(userMock);
		updateUser.setStatus(ok);
		verify(userMock).put("status", ok);
		verify(userMock).put("sentOk", true);
		verify(userMock).saveInBackground();
		}

}
