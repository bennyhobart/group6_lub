

package com.lub.ui;

import android.test.ActivityInstrumentationTestCase2;

import com.jayway.android.robotium.solo.Solo;
import com.lub.MenuActivity;
import com.lub.login.LoginActivity;
import com.lub.messenger.AlertActivity;
import com.lub.messenger.AlertCancelledActivity;
import com.lub.messenger.MessengerActivity;

/**
*Created by Leanne Capewell
*Email: capewell.leanne@gmail.com
*Created on 05/10/2013
 */

/**
 * Functional tests for the menu screen
 * 
 * Checks that each of the buttons work, and go to the correct screen
 *
 */
public class MenuActivityTest extends
		ActivityInstrumentationTestCase2<LoginActivity> {

	private Solo solo;
	private String username;
	private String password;
	private String okButton;
	private String sendOkButton;
	private String alertButton;
	private String cancelAlertButton;
	private String loginButton;
	private String logoutButton;

	public MenuActivityTest() {
		super(LoginActivity.class);

	}

	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());
		username = "testuser";
		password = "testpassword";
		okButton = "OK";
		sendOkButton = "Send OK";
		alertButton = "Send Alert";
		cancelAlertButton = "Cancel Alert";
		loginButton = "Login";
		logoutButton = "Logout";
	}

	/**
	 * Login to the app, and proceed to the menu screen
	 * Send and cancel an alert, and send an ok message.
	 */
		public void testMessageSending() {
			
			// Login
			solo.enterText(0, username);
			solo.enterText(1, password);
			solo.clickOnButton(loginButton);
			solo.assertCurrentActivity("MenuActivity not displayed after valid login", MenuActivity.class);
			
			//Send an Ok message
			solo.clickOnButton(sendOkButton);
			solo.assertCurrentActivity("Ok message sent activity not displayed after 'send OK' is pressed",
					MessengerActivity.class);
			solo.clickOnButton(okButton);
			solo.assertCurrentActivity("Menu activity not displayed after 'OK' is pressed",
					MenuActivity.class);
			
			//Send an Alert
			solo.clickOnButton(alertButton);
			solo.assertCurrentActivity("Alert sent activity not displayed after 'send alert' is pressed",
					AlertActivity.class);
			solo.clickOnButton(okButton);
			solo.assertCurrentActivity("Menu activity not displayed after 'OK' is pressed",
					MenuActivity.class);
			
			//Cancel an Alert
			solo.clickOnButton(cancelAlertButton);
			solo.assertCurrentActivity("Alert cancelled activity not displayed after 'cancel alert' is pressed",
					AlertCancelledActivity.class);
			solo.clickOnButton(okButton);
			solo.assertCurrentActivity("Menu activity not displayed after 'OK' is pressed",
					MenuActivity.class);
			solo.goBack();
			solo.clickOnButton(logoutButton);
			
		}

}
