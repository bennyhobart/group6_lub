/**
Created by Leanne Capewell
Email: capewell.leanne@gmail.com
Created on 04/09/2013
 */

package com.lub.ui;

import android.test.ActivityInstrumentationTestCase2;

import com.jayway.android.robotium.solo.Solo;
import com.lub.MenuActivity;
import com.lub.login.LoginActivity;
import com.lub.login.RegisterActivity;

/**
 * Functional tests for the login screen
 * 
 * Checks that each of the buttons work, and go to the correct screen
 * Also checks that validation is working correctly.
 *
 */
public class LoginActivityTest extends
		ActivityInstrumentationTestCase2<LoginActivity> {

	private Solo solo;

	String validUsername;
	String validPassword;
	String invalidUsername;
	String invalidPassword;
	String loginButton;
	String registerButton;
	String thisActivity = "LoginActivity";

	public LoginActivityTest() {
		super(LoginActivity.class);

	}

	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());
		validUsername = "benny";
		validPassword = "abc123";
		invalidUsername = "a";
		invalidPassword = "b";
		loginButton = "Login";
		registerButton = "Register";
	}


	// Given that the login screen is displayed and a correct
	// username and password has been entered,
	// when the user taps login,
	// then the menu screen displays.
	public void testValidUserLogin() {
		solo.enterText(0, validUsername);
		solo.enterText(1, validPassword);
		solo.clickOnButton(loginButton);
		solo.assertCurrentActivity(
				"MenuActivity not displayed after valid login",
				MenuActivity.class);
		solo.goBackToActivity(thisActivity);
	}

	// Given that the login screen is displayed and an invalid username has been
	// entered,
	// when the user presses login,
	// then an error message displays.
	public void testInvalidUserLogin() {
		solo.enterText(0, invalidUsername);
		solo.enterText(1, validPassword);
		solo.clickOnButton(loginButton);
		solo.assertCurrentActivity(
				"LoginActivity not displayed after invalid username",
				LoginActivity.class);
	}

	// Given that the login screen is displayed and an invalid password has been
	// entered,
	// when the user presses login,
	// then an error message displays.
	public void testInvalidPasswordLogin() {
		solo.enterText(0, validUsername);
		solo.enterText(1, invalidPassword);
		solo.clickOnButton(loginButton);
		solo.assertCurrentActivity(
				"LoginActivity not displayed after invalid password",
				LoginActivity.class);
	}

	// Given that the login screen is displayed,
	// when the user presses register,
	// the register screen displays.
	public void testRegisterButton() {
		solo.clickOnButton(registerButton);
		solo.assertCurrentActivity(
				"RegisterActivity not displayed after register button clicked",
				RegisterActivity.class);

		solo.goBackToActivity(thisActivity);
	}
}
