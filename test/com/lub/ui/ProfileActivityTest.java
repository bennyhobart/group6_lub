/**
Created by Leanne Capewell
Email: capewell.leanne@gmail.com
Created on 22/09/2013
 */

package com.lub.ui;

import android.test.ActivityInstrumentationTestCase2;

import com.jayway.android.robotium.solo.Solo;
import com.lub.MenuActivity;
import com.lub.login.LoginActivity;
/**
 * Functional tests for the Profile screen that checks that the ui correctly
 * displays the user's details, and the user is able to update their details
 */
public class ProfileActivityTest extends
		ActivityInstrumentationTestCase2<LoginActivity> {

	private Solo solo;
	private String oldFirstName;
	private String newFirstName;
	private String username;
	private String password;
	private String saveButton;
	private String cancelButton;
	private String profileButton;
	private String loginButton;

	public ProfileActivityTest() {
		super(LoginActivity.class);
	}

	protected void setUp() throws Exception {
		super.setUp();
		solo = new Solo(getInstrumentation(), getActivity());
		newFirstName = "new";
		oldFirstName = "Test";
		username = "testuser";
		password = "testpassword";
		saveButton = "Save";
		cancelButton = "Back";
		profileButton = "Profile";
		loginButton = "Login";
	}

/**
 * Login to the app, and proceed to the profile screen
 * Change the user's name and check that the change is saved
 */
	public void testProfileFunctionality() {
		
		// Login
		solo.enterText(0, username);
		solo.enterText(1, password);
		solo.clickOnButton(loginButton);
		solo.assertCurrentActivity(
				"MenuActivity not displayed after valid login",
				MenuActivity.class);
		
		// Go to profile screen
		solo.clickOnButton(profileButton);
		
		// Save new name
		solo.clearEditText(0);
		solo.enterText(0, newFirstName);
		solo.clickOnButton(saveButton);
		solo.assertCurrentActivity("MenuActivity not displayed after save is pressed",
				MenuActivity.class);
		
		// Check new name appears
		solo.clickOnButton(profileButton);
		assertTrue("New firstname is not displayed", solo.searchEditText(newFirstName));
		
		// Save the old name again
		solo.clearEditText(0);
		solo.enterText(0, oldFirstName);
		solo.clickOnButton(saveButton);
		
		// Test the cancel button
		solo.clickOnButton(profileButton);
		solo.clickOnButton(cancelButton);
		solo.assertCurrentActivity("Menu screen not displayed after cancel pressed", MenuActivity.class);
	}
}

