package com.lub.user;

import junit.framework.TestCase;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Unit tests class for UserValidation in the user package
 * 
 * @author Jordan Burdett
 * 
 */
public class UserValidationTest extends TestCase {

	private String correctUsername;
	private String correctPassword;
	private String invalidLengthUsername;
	private String invalidLengthPassword;

	@BeforeClass
	protected void setUp() throws Exception {
		super.setUp();
		correctUsername = "benny";
		correctPassword = "abc123";
		invalidLengthUsername = "test";
		invalidLengthPassword = "test";
	}

	/**
	 * Test method for validateUsername method
	 */
	@Test
	public void testValidateUsername() {

		assertTrue("Username is less than 5 characters",
				UserValidation.validateUsername(correctUsername));
		assertFalse("Username is at least 5 characters",
				UserValidation.validateUsername(invalidLengthUsername));
	}

	/**
	 * Test method for validatePassword method
	 */
	@Test
	public void testValidatePassword() {

		assertTrue("Username is less than 6 characters",
				UserValidation.validatePassword(correctPassword));
		assertFalse("Username is at least 6 characters",
				UserValidation.validatePassword(invalidLengthPassword));
	}
}
