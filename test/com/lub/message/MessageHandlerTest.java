/**
Created by Leanne Capewell
Email: capewell.leanne@gmail.com
Created on 19/09/2013
*/

package com.lub.message;


import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.anyInt;
import static org.mockito.Mockito.never;

import org.junit.Test;

import android.test.InstrumentationTestCase;

import com.lub.R;
import com.lub.friend.database.UpdateUser;
import com.lub.notification.NotificationService;
import com.lub.user.Person;

/**
 * Unit tests for the MessageHandler class
 *
 */
public class MessageHandlerTest extends InstrumentationTestCase {
	
	private UpdateUser updateUserMock;
	private NotificationService notificationServiceMock;
	private MessageHandler messageHandler;
	private Person person;
	
	private static final int ok = R.integer.ok;
	private static final int alert = R.integer.alert;
	private static final int notOk = R.integer.notOk;
	private static final String NOT_OK_MESSAGE = " has not said they are ok today";
	private static final String OK_MESSAGE = " is doing well today";
	private static final String ALERT_MESSAGE = " has sent an alert";
	private static final String CANCEL_MESSAGE = " has cancelled their alert";
	

	public void setUp() throws Exception {
		super.setUp();
		System.setProperty("dexmaker.dexcache", getInstrumentation().getTargetContext().getCacheDir().getPath());
		updateUserMock = mock(UpdateUser.class);
		notificationServiceMock = mock(NotificationService.class);
		messageHandler = new MessageHandler(updateUserMock, notificationServiceMock);
	}
	
	@Test
	public void testSendOkMessageWhenNotOk(){
		// setup
		person = new Person();
		person.setStatus(alert);
		when(updateUserMock.getCurrentPerson()).thenReturn(person);
		
		// test
		messageHandler.sendOkMessage();

		// verify
		verify(updateUserMock).setStatus(ok);
		verify(notificationServiceMock).pushNotificationToAllFriends(OK_MESSAGE);
	}
	
	@Test
	public void testSendOkMessageWhenOk(){
		// setup
		person = new Person();
		person.setStatus(ok);
		when(updateUserMock.getCurrentPerson()).thenReturn(person);
		
		// test
		messageHandler.sendOkMessage();
		
		// verify
		verify(updateUserMock, never()).setStatus(anyInt());
		verify(notificationServiceMock).pushNotificationToAllFriends(OK_MESSAGE);
	}
	
	@Test
	public void testSendAlert(){
		messageHandler.sendAlert();
		verify(updateUserMock).setStatus(alert);
		verify(notificationServiceMock).pushNotificationToAllFriends(ALERT_MESSAGE);
	}
	
	@Test
	public void testSendNoOkMEssage(){
		messageHandler.sendNoOkMessage();
		verify(updateUserMock).setStatus(notOk);
		verify(notificationServiceMock).pushNotificationToAllFriends(NOT_OK_MESSAGE);
	}
	
	@Test
	public void testCancelAlert(){
		messageHandler.cancelAlert();
		verify(updateUserMock).setStatus(ok);
		verify(notificationServiceMock).pushNotificationToAllFriends(CANCEL_MESSAGE);
	}
	
}
